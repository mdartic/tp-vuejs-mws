/* eslint-disable import/no-extraneous-dependencies */
import { configure, addDecorator } from '@storybook/vue';
import '../../src/plugins/vuetify';

const req = require.context('../../src/', true, /.stories.js$/);

addDecorator(() => ({
  template: '<v-app><story></story></v-app>',
}));

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
