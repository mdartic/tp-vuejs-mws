/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue';

import AlarmList from './AlarmList.vue';

const alarms = [{
  application: 'alarm',
  app_object: 'feu',
  group: 0,
  instance: 1234,
  creation_time: '23-08-2016 13:47:20',
  title: 'détection feu',
  description: 'feu batiment B couloir 1er étage',
  location: '9',
  location_gps: [],
  marker_profile: 'fire_profile',
  status: 99,
  status_label: 'En cours',
  priority: 1,
  actions: [{
    action_label: 'Prendre en compte',
    action: 'ack',
  },
  {
    action_label: 'Arrêter',
    action: 'stop',
  }],
},
{
  application: 'alarm',
  app_object: 'parking',
  group: 2,
  instance: 3456,
  creation_time: '02-09-2016 11:06:20',
  title: 'Incident parking',
  description: 'Barrière bloquée',
  location: '10',
  location_gps: [47.360767,
    0.765099],
  marker_profile: 'alarm_profile',
  status: 0,
  status_label: 'Succès',
  actions: [{
    action_label: 'Prendre en compte',
    action: 'ack',
  },
  {
    action_label: 'Arrêter',
    action: 'stop',
  }],
},
{
  application: 'alarm',
  app_object: 'parking',
  group: 2,
  instance: 34560,
  creation_time: '02-09-2016 11:06:20',
  title: 'Incident parking 2',
  description: 'Barrière bloquée 2',
  location: '1',
  location_gps: [47.360727,
    0.765039],
  marker_profile: 'fire_profile',
  status: 0,
  status_label: 'Succès',
  actions: [{
    action_label: 'Prendre en compte',
    action: 'ack',
  },
  {
    action_label: 'Arrêter',
    action: 'stop',
  }],
},
{
  application: 'alarm',
  app_object: 'parking',
  group: 2,
  instance: 34561,
  creation_time: '02-09-2016 11:06:20',
  title: 'Incident parking 3',
  description: 'Barrière bloquée 3',
  location: '2',
  location_gps: [47.360767,
    0.765099],
  marker_profile: 'fire_profile',
  status: 0,
  status_label: 'Succès',
  actions: [{
    action_label: 'Prendre en compte',
    action: 'ack',
  },
  {
    action_label: 'Arrêter',
    action: 'stop',
  }],
},
{
  application: 'alarm',
  app_object: 'parking',
  group: 2,
  instance: 34563,
  creation_time: '02-09-2016 11:06:20',
  title: 'Incident parking 2',
  description: 'Barrière bloquée 2',
  location: '3',
  location_gps: [47.360667,
    0.765299],
  marker_profile: 'alarm_profile',
  status: 0,
  status_label: 'Succès',
  actions: [{
    action_label: 'Prendre en compte',
    action: 'ack',
  },
  {
    action_label: 'Arrêter',
    action: 'stop',
  }],
},
{
  application: 'alarm',
  app_object: 'parking',
  group: 2,
  instance: 34562,
  creation_time: '02-09-2016 11:06:20',
  title: 'Incident parking 2',
  description: 'Barrière bloquée 2',
  location: '74',
  location_gps: [47.360957,
    0.765409],
  marker_profile: 'alarm_profile',
  status: 0,
  status_label: 'Succès',
  actions: [{
    action_label: 'Prendre en compte',
    action: 'ack',
  },
  {
    action_label: 'Arrêter',
    action: 'stop',
  }],
},
{
  application: 'apc',
  app_object: 'CPA Lucien Martineau',
  group: 1,
  instance: 97522,
  creation_time: '02-09-2016 16:52:41',
  title: 'Lucien Martineau',
  description: 'Surveillance intervention technicien',
  location: '75',
  location_gps: [47.35534,
    0.72708],
  marker_profile: 'apc_profile',
  status: 99,
  status_label: 'En cours',
  actions: [{
    action_label: 'Stopper surveillance',
    action: 'stop',
  }],
}];

storiesOf('Components|AlarmList', module)
  .add('default', () => ({
    components: { AlarmList },
    render() {
      return (
        <v-container justify-center>
          <v-layout>
            <v-flex xs12 md8 align-center>
              <alarm-list />
            </v-flex>
          </v-layout>
        </v-container>
      );
    },
  }))
  .add('with items', () => ({
    components: { AlarmList },
    render() {
      return (
        <v-container justify-center>
          <v-layout>
            <v-flex xs12 md8 align-center>
              <alarm-list alarms={alarms} />
            </v-flex>
          </v-layout>
        </v-container>
      );
    },
  }))
  .add('loading', () => ({
    components: { AlarmList },
    render() {
      return (
        <v-container justify-center>
          <v-layout>
            <v-flex xs12 md8 align-center>
              <alarm-list loading={true} />
            </v-flex>
          </v-layout>
        </v-container>
      );
    },
  }));
