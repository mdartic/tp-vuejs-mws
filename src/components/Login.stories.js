/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';

import LoginComponent from './Login.vue';

storiesOf('LoginComponent', module)
  .add('with text', () => ({
    components: { LoginComponent },
    template: '<login-component @submit="action" />',
    methods: { action: action('clicked') },
  }));
