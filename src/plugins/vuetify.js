import Vue from 'vue';
import Vuetify, {
  VApp,
  VContainer,
  VLayout,
  VBtn,
  VForm,
  VTextField,
  VFlex,
  VTreeview,
  VIcon,
  VDataTable,
} from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';
import fr from 'vuetify/es5/locale/fr';
import 'material-design-icons-iconfont/dist/material-design-icons.css';

Vue.use(Vuetify, {
  theme: {
    primary: '#ee44aa',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
  },
  options: {
    customProperties: true,
  },
  components: {
    VApp,
    VContainer,
    VLayout,
    VBtn,
    VForm,
    VTextField,
    VFlex,
    VTreeview,
    VIcon,
    VDataTable,
  },
  iconfont: 'md',
  lang: {
    locales: { fr },
    current: 'fr',
  },
});
