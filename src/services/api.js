import axios from 'axios';
import PreferencesService from '@/services/preferences';

function getRequestParameters() {
  if (!PreferencesService.user) return {};
  return {
    SESSID: PreferencesService.user.SESSID,
  };
}

/**
 * Function to be executed when a timeout is trigerred by the backend
 * @type {Function}
 */
let timeoutFunction = null;

/**
 * Create the axios instance,
 * it will be used each time an API call have to be done
 */
const axiosInstance = axios.create({
  baseURL: `${process.env.VUE_APP_SITE_API_URL}/server`,
  timeout: 0,
});

/**
 * Inject params in the request,
 * eg. the SESSID retrieved from local storage
 */
axiosInstance.interceptors.request.use(config => ({
  ...config,
  params: {
    ...config.params,
    ...getRequestParameters(),
  },
}));

/**
 * Change the response if timeout is detected
 * The app will have to redirect to home page
 */
axiosInstance.interceptors.response.use((response) => {
  if (response.data.timeout === 'true') {
    console.log('session is over, => go back to home');
    if (timeoutFunction) timeoutFunction('Votre session est terminée, merci de vous authentifier à nouveau');
  }
  return response;
}, error => Promise.reject(error));


export default {
  /**
   * Log the user and return an object
   * @param {string} login User login
   * @param {string} password User password
   * @returns {Promise}
   */
  login(login, password) {
    const formData = new FormData();
    formData.set('user', login);
    formData.set('passwd', password);
    return axiosInstance.request({
      method: 'post',
      url: '/login/check_login.php',
      data: formData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } },
    });
  },

  loadImages() {
    return axiosInstance.request({
      method: 'get',
      url: '/maps/images/load_images.php',
    });
  },

  loadImagesFiles() {
    return axiosInstance.request({
      method: 'get',
      url: '/maps/images/load_images_files.php',
    });
  },

  loadPolygons(imageId) {
    return axiosInstance.request({
      method: 'get',
      url: '/maps/polygons/load_polygons.php',
      params: {
        image_id: imageId,
      },
    });
  },

  convertSound(file) {
    return axiosInstance.request({
      method: 'get',
      url: '/utils/alaw2wav.php',
      params: {
        file,
      },
    });
  },

  loadRefPoints() {
    return axiosInstance.request({
      method: 'get',
      url: '/maps/ref_points/load_points.php',
    });
  },

  loadManagementUnits() {
    return axiosInstance.request({
      method: 'get',
      url: '/users/load_user_mngt_units.php',
    });
  },

  loadEvents() {
    return axiosInstance.request({
      method: 'get',
      baseURL: '/',
      url: '/data/events.json',
    });
  },

  loadData() {
    return axiosInstance.request({
      method: 'get',
      url: '/maps/load_data.php',
    });
  },

  loadMarkers() {
    return axiosInstance.request({
      method: 'get',
      url: '/maps/markers/load_markers.php',
    });
  },

  /**
   * Trigger an action on an alarm
   *
   * @param {string} action
   * Action type, like ack, stop, ...
   * Given by the load_data call
   * @param {string} alarmId
   * Id of the alarm for which the action will be trigerred (instance property)
   */
  triggerActionOnAlarm(action, alarmId) {
    return axiosInstance.request({
      method: 'get',
      url: '/maps/send_button_action.php',
      params: {
        action,
        alarm_id: alarmId,
      },
    });
  },

  /**
   * Register a function that will be executed
   * when the API Backend will trigerred us a timeout
   *
   * @param {Function} f Function to be executed when timeout is trigerred by backend API
   */
  onTimeout(f) {
    timeoutFunction = f;
  },
};
