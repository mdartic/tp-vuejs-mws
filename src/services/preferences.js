/* eslint-disable no-underscore-dangle */

const STORAGE_KEY_SETTINGS = process.env.VUE_APP_STORAGE_KEY_SETTINGS;

/**
 * User of MWS (TS Definition for IntelliSense)
 * @typedef MWSUser
 * @property {string} id Id of the user
 * @property {string} name Name of the user
 * @property {string} SESSID Session id
 * @property {[number]} mus_id
 * Array of management unit allowed for the user
 * @property {[number]} alarms_groups
 * Array of alarms groups allowed for the user
 * @property {string} licTamatV5
 * Licence for the user :
 * * 1 = maps
 * * 2 = plans
 */

const defaultUser = {
  mus_id: [],
  alarms_groups: [],
};
const defaultPreferences = JSON.stringify({
  user: { ...defaultUser },
});

export default {
  _data: JSON.parse(localStorage.getItem(STORAGE_KEY_SETTINGS) || defaultPreferences),

  updateData(newData) {
    try {
      const dataJSON = JSON.stringify(newData);
      localStorage.setItem(STORAGE_KEY_SETTINGS, dataJSON);
      this._data = newData;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * @type {MWSUser}
   */
  get user() {
    return this._data.user;
  },

  set user(newValue) {
    this.updateData({
      ...this._data,
      user: newValue || { ...defaultUser },
    });
  },
};
