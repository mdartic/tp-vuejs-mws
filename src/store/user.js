import ApiService from '@/services/api';
import PreferencesService from '@/services/preferences';
import storeShared from './shared';

const store = {
  name: 'user',
  state: {
    loading: false,
    error: null,
    isLogged: PreferencesService.user !== null,
    user: PreferencesService.user,
  },
  actions: {
    async logUser(login, password) {
      store.state.loading = true;
      store.state.error = null;
      try {
        const userResponse = await ApiService.login(login, password);
        if (userResponse.data.success === 'false') {
          store.state.isLogged = false;
          if (
            userResponse.data.error === '1001'
            || userResponse.data.error === '1000'
          ) {
            store.state.error = 'Utilisateur et/ou mot de passe incorrect.';
          } else {
            store.state.error = userResponse.data.error;
          }
        } else {
          store.state.isLogged = true;
          /**
           * we transform mus_id & alarms_groups to be more useable in the code
           */
          store.state.user = {
            ...userResponse.data,
            // eslint-disable-next-line radix
            mus_id: userResponse.data.mus_id.split(',').map(muid => parseInt(muid)),
            // eslint-disable-next-line radix
            alarms_groups: userResponse.data.alarms_groups.split(',').map(alarm => parseInt(alarm)),
          };
          PreferencesService.user = store.state.user;
          storeShared.actions.resetFilters();
          storeShared.actions.resetMode();
        }
      } catch (e) {
        store.state.isLogged = false;
        store.state.error = e;
      }
      store.state.loading = false;
    },
    logout() {
      store.state.error = null;
      store.state.isLogged = false;
      PreferencesService.user = null;
      store.state.user = PreferencesService.user;
    },
  },
};

export default store;
