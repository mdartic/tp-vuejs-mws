import ApiService from '@/services/api';

const interval = 3000; // ms

const store = {
  name: 'alarms',
  state: {
    loading: false,
    error: null,
    results: [],
    intervalId: null,
  },
  actions: {
    async fetchAlarms() {
      store.state.loading = true;
      try {
        const dataResponse = await ApiService.loadData();
        store.state.results = dataResponse.data;
      } catch (e) {
        store.state.error = e;
      }
      store.state.loading = false;
    },

    /**
     * Schedule the alarm fetch every 3s
     */
    scheduleTimerAlarm() {
      store.state.intervalId = setInterval(() => {
        store.actions.fetchAlarms();
      }, interval);
    },

    /**
     * Stop the alarm timer
     */
    shutdownTimerAlarm() {
      clearInterval(store.state.intervalId);
    },

  },
};

export default store;
