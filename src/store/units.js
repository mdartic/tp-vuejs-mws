import ApiService from '@/services/api';

const store = {
  name: 'units',
  state: {
    loading: false,
    error: null,
    results: null,
  },
  computed: {
    /**
     * Find the unit by his id and return it, or {} if not found
     * @param {string} id id of the unit
     * @returns {Object}
     */
    findUnit(id) {
      if (!store.state.results) return {};
      // eslint-disable-next-line radix
      return store.state.results.find(r => parseInt(r.id) === parseInt(id)) || {};
    },
  },
  actions: {
    async fetchManagementUnits() {
      store.state.loading = true;
      try {
        const managementUnits = await ApiService.loadManagementUnits();
        store.state.results = managementUnits.data.map(u => ({
          ...u,
          // eslint-disable-next-line radix
          id: parseInt(u.id),
        }));
      } catch (e) {
        store.state.error = e;
      }
      store.state.loading = false;
    },
  },
};


export default store;
