import ApiService from '@/services/api';

const store = {
  name: 'markers',
  state: {
    loading: false,
    error: null,
    results: [],
  },
  computed: {
    /**
     * Find the marker by his id and return it, or {} if not found
     * @param {string} id id of the marker
     * @returns {Object}
     */
    findMarker(id) {
      // eslint-disable-next-line radix
      return store.state.results.find(r => parseInt(r.id) === parseInt(id)) || {};
    },
  },
  actions: {
    /**
     * Fetch the TAMAT Markers
     */
    async fetchMarkers() {
      store.state.loading = true;
      try {
        const markersResponse = await ApiService.loadMarkers();
        store.state.results = markersResponse.data.map(m => ({
          ...m,
          symbol: m.symbol !== '' ? process.env.VUE_APP_SITE_API_URL + m.symbol : '',
        }));
      } catch (e) {
        store.state.error = e;
      }
      store.state.loading = false;
    },
  },
};

export default store;
