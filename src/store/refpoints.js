import ApiService from '@/services/api';
import storeShared from './shared';

const store = {
  name: 'points',
  state: {
    loading: false,
    error: null,
    results: null,
    localPoints: null,
  },
  computed: {
    currentPolygonPoints(currentPolygonId) {
      if (!store.state.results) return [];
      return store.state.results.filter(d => d.polygonId === currentPolygonId);
    },
    imagePoints() {
      if (!store.state.results) return [];
      return store.state.results.filter(r => r.image_id === storeShared.state.imageId);
    },
  },
  actions: {
    async fetchRefPoints() {
      store.state.loading = true;
      try {
        const refPointsResponse = await ApiService.loadRefPoints();
        store.state.results = refPointsResponse.data;
      } catch (e) {
        store.state.error = e;
      }
      store.state.loading = false;
    },
  },
};

export default store;
