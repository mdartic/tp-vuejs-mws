/* global MAIN_URL */
import ApiService from '@/services/api';
import storeShared from './shared';

const store = {
  name: 'images',
  state: {
    /** @type {Boolean} */
    loading: false,
    /** @type {Error} */
    error: null,
    /** @type {Array} */
    results: [],
    /** @type {Array} */
    polygons: [],
    /** @type {Array} */
    imagesFiles: [],
    /** @type {Object} */
    topImage: null,
    /** @type {Object} */
    defaultImage: null,
  },
  computed: {
    /** @return {Object} */
    currentImage() {
      if (!store.state.results) return null;
      return store.state.results.find(r => r.id === storeShared.state.imageId) || {};
    },
    /** @return {String} */
    currentImageName() {
      return store.computed.currentImage() ? store.computed.currentImage().name : 'No image selected';
    },
  },
  actions: {
    async fetchPolygons() {
      store.state.loading = true;
      try {
        const response = await ApiService.loadPolygons();
        store.state.polygons = response.data;
      } catch (e) {
        store.state.error = e;
      }
      store.state.loading = false;
    },
    async fetchImages() {
      store.state.loading = true;
      try {
        const imagesResponse = await ApiService.loadImages();
        // we update the path with MAIN_URL to get the full image url
        // @TODO update API ? to get a full path instead of a relative one
        store.state.results = imagesResponse.data.map(r => ({
          ...r,
          path: process.env.VUE_APP_SITE_API_URL + r.path,
        }));

        if (store.state.results.length > 0) {
          store.state.results.forEach((image) => {
            if (image.parent_id === '0') {
              // Display the default image
              store.state.defaultImage = image;
              store.state.topImage = image;

              storeShared.state.imageId = image.id;
            }
          });
        }
      } catch (e) {
        store.state.error = e;
      }
      store.state.loading = false;
    },
  },
};

/**
 * Fetch all available polygons
 */
export async function fetchPolygons() {
  store.state.loading = true;
  try {
    const response = await ApiService.loadPolygons();
    store.state.polygons = response.data;
  } catch (e) {
    store.state.error = e;
  }
  store.state.loading = false;
}

/**
 * Fetch all available images
 */
export async function fetchImages() {
  store.state.loading = true;
  try {
    const imagesResponseFiles = await ApiService.loadImagesFiles();
    store.state.imagesFiles = imagesResponseFiles.data;

    const imagesResponse = await ApiService.loadImages();
    // we update the path with MAIN_URL to get the full image url
    // @TODO update API ? to get a full path instead of a relative one
    store.state.results = imagesResponse.data.map(r => ({
      ...r,
      path: MAIN_URL + r.path,
    }));

    if (store.state.results.length > 0) {
      store.state.results.forEach((image) => {
        if (image.parent_id === '0') {
          // Display the default image
          store.state.defaultImage = image;
          store.state.topImage = image;

          storeShared.state.imageId = image.id;
        }
      });
    }
  } catch (e) {
    store.state.error = e;
  }
  store.state.loading = false;
}

export default store;
