import VueReactiveStore from 'vue-reactive-store';
// import VRSPluginDevtools from 'vue-reactive-store/dist/devtools.esm';

import storeAlarms from './alarms';
import storeImages from './images';
import storeMarkers from './markers';
import storePoints from './refpoints';
import storeShared from './shared';
import storeUnits from './units';
import storeUser from './user';

function getChildren(itemId) {
  return storeImages.state.results.filter(i => i.parent_id === itemId).map((i) => {
    const currentPoints = storePoints.state.results.filter(
      r => r.image_id.toString() === i.id.toString(),
    ) || [];
    const currentEvents = currentPoints.reduce((acc, p) => {
      /**
       * We try to find an event with the same id
       */
      const event = storeAlarms.state.results.find(r => r.location === p.id);
      /**
       * We check the event found (or not) is ok with shared filters
       */
      if (event === undefined) { return acc; }
      const marker = storeMarkers.state.results.find(
        // eslint-disable-next-line radix
        r => parseInt(r.id) === parseInt(event.marker_profile),
      ) || {};
      // we check if the "status_visible" property of the marker is ok (equal to 1 = to display)
      if (marker[`${event.status}_visible`] !== '1') return acc;

      return acc.concat({
        ...p,
        ...event,
        marker: {
          iconUrl: marker.symbol,
          className: marker.shape,
          color: marker[`${event.status}_color`],
        },
      });
    }, []);
    const children = getChildren(i.id);
    const childrenPoints = children.reduce((acc, c) => (acc + c.points), 0);
    const childrenEvents = children.reduce((acc, c) => (acc + c.events), 0);
    /**
     * We try to find polygons related to the image
     */
    const polygons = storeImages.state.polygons.filter(
      p => p.image_id === i.id,
    ).map((p) => {
      const childrenImage = children.find(c => c.id === p.child_image_id);
      return {
        ...p,
        label: childrenImage ? childrenImage.events : null,
      };
    });
    return {
      ...i,
      polygons,
      points: currentPoints.length + childrenPoints,
      events: currentEvents.length + childrenEvents,
      currentEvents,
      children,
    };
  });
}

/**
 * Return the image of a hierarchial element with the right id
 * @param {Object} element Element to dig to find the image
 * @param {string} imageId id of the image we try to find
 */
function getImage(element, imageId) {
  if (element.id === imageId) return element;

  let result = null;
  if (element.children) {
    element.children.forEach((e) => {
      const currentImage = getImage(e, imageId);
      if (currentImage) result = currentImage;
    });
  }
  return result;
}

/**
 * This store use all the sub stores
 * and expose all computed properties that need data across sub-stores
 */
const store = {
  name: 'mws-store',
  state: {},
  computed: {
    imageTreeWithPoints() {
      if (
        !storeImages.state.results
        || !storePoints.state.results
        || !storeMarkers.state.results
        || !storeAlarms.state.results
      ) return [];
      return getChildren('0');
    },
    currentImageTreeWithPoints() {
      if (store.computed.imageTreeWithPoints().length === 0) return {};
      return getImage(store.computed.imageTreeWithPoints()[0], storeShared.state.imageId) || {};
    },
    alarmsToDisplay() {
      if (!storeAlarms.state.results) return [];
      return storeAlarms.state.results.map((alarm) => {
        const unit = storeUnits.computed.findUnit(alarm.mu_id);
        // eslint-disable-next-line radix
        const currentMarker = storeMarkers.computed.findMarker(alarm.marker_profile);
        return {
          ...alarm,
          unit,
          marker: {
            iconUrl: currentMarker.symbol,
            className: currentMarker.shape,
            color: currentMarker[`${alarm.status}_color`],
          },
        };
      });
    },
    /**
     * Points (markers) to display on the plan
     */
    imagePointsToDisplay() {
      if (!storeAlarms.state.results) return [];
      // eslint-disable-next-line array-callback-return
      return storePoints.computed.imagePoints().reduce((acc, p) => {
        /**
         * We try to find an alarm with the same id
         */
        const alarm = storeAlarms.state.results.find(r => r.location === p.id);
        if (alarm === undefined) { return acc; }
        const marker = storeMarkers.computed.findMarker(alarm.marker_profile);
        return acc.concat({
          ...p,
          ...alarm,
          marker: {
            iconUrl: marker.symbol,
            className: marker.shape,
            color: marker[`${alarm.status}_color`],
          },
        });
      }, []);
    },
    /**
     * Polygons to display on map / plan
     * @returns {Array}
     */
    polygonsToDisplay() {
      return storeImages.state.polygons.filter(
        p => p.image_id === storeShared.state.imageId,
      ).map((p) => {
        const image = getImage(store.computed.currentImageTreeWithPoints(), p.child_image_id);
        return {
          ...p,
          label: image ? image.events : null,
        };
      });
    },
  },
  modules: {
    alarms: storeAlarms,
    images: storeImages,
    markers: storeMarkers,
    points: storePoints,
    shared: storeShared,
    units: storeUnits,
    user: storeUser,
  },
  plugins: [
    // VRSPluginDevtools,
  ],
};

// make the store reactive
// eslint-disable-next-line no-new
new VueReactiveStore(store);

export default store;
