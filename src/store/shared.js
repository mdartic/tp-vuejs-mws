import PreferenceService from '@/services/preferences';

/**
 * Application mode,
 */
export const APP_MODE = {
  PLAN: 0,
  MAP: 1,
};

function getDefaultFiltersValues() {
  return {
    status: ['present', 'ack', 'absent'],
    alarms: [...PreferenceService.user.alarms_groups],
    managementUnits: [...PreferenceService.user.mus_id],
  };
}

export const store = {
  name: 'shared',
  state: {
    /**
     * When imageId is set,
     * * computed property of polygonsToDisplay will be updated
     * * computed property of ref points will be updated
     * @param {string} newValue
     */
    imageId: null,
    /**
     * Filters are used to filter data displayed :
     * * points / alarms on map
     * * alarms on datatable
     */
    filters: {
      values: {
      },
      items: {
        alarms: [{
          value: 0,
          label: '0',
        }, {
          value: 1,
          label: '1',
        }, {
          value: 2,
          label: '2',
        }, {
          value: 3,
          label: '3',
        }, {
          value: 4,
          label: '4',
        }, {
          value: 5,
          label: '5',
        }, {
          value: 6,
          label: '6',
        }, {
          value: 7,
          label: '7',
        }, {
          value: 8,
          label: '8',
        }, {
          value: 9,
          label: '9',
        }],
        status: [{
          value: 'present',
          label: 'Démarrée',
          disabled: true,
        }, {
          value: 'ack',
          label: 'Prise en compte',
          disabled: false,
        }, {
          value: 'absent',
          label: 'Terminée',
          disabled: false,
        }],
      },
    },
    /**
     * application mode : plan or map
     */
    mode: APP_MODE.PLAN,
  },
  computed: {
    isInModePlan() {
      return store.state.mode === APP_MODE.PLAN;
    },
    isInModeMap() {
      return store.state.mode === APP_MODE.MAP;
    },
    checkFiltersItem(id, filter) {
      if (!store.state.filters.values[filter]) return false;
      return (
        store.state.filters.values[filter].length === 0
        // eslint-disable-next-line radix
        || store.state.filters.values[filter].findIndex(e => e === id) !== -1
      );
    },
    isStatusOK(id) {
      return store.computed.checkFiltersItem(id, 'status');
    },
    isAlarmOK(id) {
      return store.computed.checkFiltersItem(id, 'alarms');
    },
    isMUOK(id) {
      return store.computed.checkFiltersItem(id, 'managementUnits');
    },
  },
  actions: {
    resetFilters() {
      const defaultFiltersValues = getDefaultFiltersValues();
      Object.keys(defaultFiltersValues).forEach((key) => {
        store.state.filters.values[key] = [...defaultFiltersValues[key]];
      });
    },
    resetMode() {
      store.state.mode = APP_MODE.PLAN;
    },
  },
};

store.actions.resetFilters();

export default store;
